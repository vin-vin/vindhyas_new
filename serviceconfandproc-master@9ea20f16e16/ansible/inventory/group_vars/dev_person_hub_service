---
nexus:
  group_id: com.gs.cft.cards
  version: latest
  repo: master

database:
  connection:
    username: cards_person_hub_dev_user
    password: cards_person_hub_dev_pass
    replicaSet: "{{mongodbReplicaSets.main}}"
    databaseName: cards_person_hub_dev

crypto:
  vault:
    connection:
      host: vault.infra.marcus.com
      port: 8200
      role_id: b7a66aa0-01ee-ad84-ff40-691cd9ec74a0
      secret_id: a29fbe7f-aec3-ba6e-d6ec-a27d440e2ded
    secrets:
     - name: tokenDecrypt
       location: keys/export/encryption-key/dev-key
     - name: entityEncryptionKey
       location: keys/export/encryption-key/dev-key
     - name: searchableFieldHasherSalt
       location: keys/export/encryption-key/dev-key
  hashers:
   - name: searchableFieldHasher
     secret: searchableFieldHasherSalt
     algo: SHA-512
  encryptors:
   - name: entityEncryptor
     secret: entityEncryptionKey
     algo: AES/GCM/NoPadding

server:
  jetty:
    accesslog:
      enabled: true
      retention_period: 15
      file_date_format: yyyy-MM-dd
      filename: "{{service_log_dir}}/{{service_dir_name}}/{{service_dir_name}}-access.log"
      time_zone: GMT
      extended_format: false
      log_latency: true
      append: true

logging:
  path: "{{service_log_dir}}/{{service_dir_name}}"
  file:
    max_history: 15
