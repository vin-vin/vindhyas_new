---
nexus:
  group_id: com.gs.cft.cards
  version: latest
  repo: master

auth:
  signingKey: test123

finalCardService:
  url: https://api.issuing.dev2.getfinal.com/
  healthCheck: http://localhost:5071/health

documentGeneration:
  application_terms_and_conditions: static-9105357d-35c4-47b2-b672-730763a25928
  partner_data_sharing: static-36dc95f0-5e54-4c69-bb98-f2614f5bd4a3
  vt: static-b1dfddb0-6880-4f57-8de1-8eceb6e8cb1c
  ca: static-45c45ca0-687f-4ad7-a802-fa9e58edb1ab

app:
  productType: CREDIT_CARD
  offerValidityPeriod:
    magnitude: 30
    unit: DAYS

#TO BE REMOVED WITH APP S7 PUSH
vault:
  connection:
    host: vault.infra.marcus.com
    port: 8200
    role_id: b7a66aa0-01ee-ad84-ff40-691cd9ec74a0
    secret_id: a29fbe7f-aec3-ba6e-d6ec-a27d440e2ded
  secrets:
    key_location: keys/export/encryption-key/dev-key
    salt_location: keys/export/encryption-key/dev-key

crypto:
  vault:
    connection:
      host: vault.infra.marcus.com
      port: 8200
      role_id: b7a66aa0-01ee-ad84-ff40-691cd9ec74a0
      secret_id: a29fbe7f-aec3-ba6e-d6ec-a27d440e2ded
    secrets:
      entityEncryptionKey:
        key_location: keys/export/encryption-key/dev-key
      searchableFieldHasherSalt:
        key_location: keys/export/encryption-key/dev-key
  hashers:
    searchableFieldHasherSalt:
      secret: searchableFieldHasherSalt
      algo: SHA-512

  encryptors:
    entityEncryptor:
      secret: searchableFieldHasherSalt
      algo: AES/GCM/NoPadding

database:
  connection:
    username: cards_application_dev_user
    password: cards_application_dev_pass
    replicaSet: "{{mongodbReplicaSets.main}}"
    databaseName: cards_application_dev

kafka:
  bootstrap_servers: "{{kafkaClusterServers}}"
  consumer:
    retry_interval: 1000
    concurrency_limit: 3
  producer:
    retries: 3
    timeout_milliseconds: 5000

server:
  jetty:
    accesslog:
      enabled: true
      retention_period: 15
      file_date_format: yyyy-MM-dd
      filename: "{{service_log_dir}}/{{service_dir_name}}/{{service_dir_name}}-access.log"
      time_zone: GMT
      extended_format: false
      log_latency: true
      append: true

swagger:
  appInfo:
    title: "DEV Card Application Service API"

logging:
  path: "{{service_log_dir}}/{{service_dir_name}}"
  file:
    max_history: 15
