---
nexus:
  group_id: com.gs.cft.data.mongo.change.streams
  artifact_id: change-stream-service
  version: latest
  repo: master

app:
  kafka:
    producer:
      timeOutMilliSeconds: 5000
  data:
    streams:
      source:
        database: cards_person_hub_patqa
        collection: persons
        uri: mongodb://cards_person_hub_patqa_user:cards_person_hub_patqa_pass@10.207.52.18:27017,10.207.52.78:27017,10.207.52.132:27017/cards_person_hub_patqa
      destination:
        database: cft_person_hub_history_patqa
        collection: persons_history
        uri: mongodb://cft_person_hub_history_patqa_user:cft_person_hub_history_patqa_pass@10.207.52.53:27017,10.207.52.91:27017,10.207.52.142:27017/cft_person_hub_history_patqa
        kafkaTopicFilterConfigs:
          - topicName: cft.change-streams.persons
            filterType: AGGREGATE

spring:
  kafka:
    bootstrap_servers: 10.207.52.10:9092,10.207.52.137:9092,10.207.52.123:9092
    producer:
      retries: 3

crypto:
  vault:
    connection:
      host: vault.infra.marcus.com
      port: 8200
      role_id: b7a66aa0-01ee-ad84-ff40-691cd9ec74a0
      secret_id: a29fbe7f-aec3-ba6e-d6ec-a27d440e2ded
    secrets:
     - name: entityEncryptionKey
       location: keys/export/encryption-key/dev-key
     - name: searchableFieldHasherSalt
       location: keys/export/encryption-key/dev-key
  hashers:
   - name: searchableFieldHasher
     secret: searchableFieldHasherSalt
     algo: SHA-512
  encryptors:
   - name: entityEncryptor
     secret: entityEncryptionKey
     algo: AES/GCM/NoPadding

server:
  jetty:
    accesslog:
      enabled: true
      retention_period: 15
      file_date_format: yyyy-MM-dd
      filename: "{{service_log_dir}}/{{service_dir_name}}/{{service_dir_name}}-access.log"
      time_zone: GMT
      extended_format: false
      log_latency: true
      append: true

logging:
  path: "{{service_log_dir}}/{{service_dir_name}}"
  file:
    max_history: 15
